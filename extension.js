/*
loadavg gnome shell extension

*/

const St = imports.gi.St;
const Main = imports.ui.main;
const Shell = imports.gi.Shell;
const Mainloop = imports.mainloop;
const Lang = imports.lang;
const PanelMenu = imports.ui.panelMenu;


function LoadAvg () {
    this._init.apply(this, arguments);
}

LoadAvg.prototype = {
	__proto__: PanelMenu.Button.prototype,


	_init: function () {
        PanelMenu.Button.prototype._init.call(this, 0.0, "LoadAvg");

        this.actor.visible = true;
        this.label = new St.Label();
        this.actor.add_actor(this.label);
        this.defaultLabelStyle = this.label.style;
        this.update();

        timeout = Mainloop.timeout_add_seconds(1, Lang.bind(this, this.update));
	},

    update: function () {
        let x = Shell.get_file_contents_utf8_sync('/proc/loadavg').split(" ")[0],
            xn = parseFloat(x);
        if (xn < 0.25) {
            this.label.style = this.defaultLabelStyle;
        } else if (xn < 0.75) {
            this.label.style = "color: yellow";
        } else {
            this.label.style = "color: red";
        }
        this.label.text = x;
        return true;
    }
}

function init () {
// pass
}

function enable () {
    widget = new LoadAvg();
    Main.panel.addToStatusArea('LoadAvg', widget, 0);
}

function disable () {
    Mainloop.source_remove(timeout);
    timeout = null;
    widget.destroy();
    widget = null;
}


let widget = null;
let timeout = null;

